/*
 * @Author: 信念D力量
 * @Github: https://www.github.com/fy2008
 * @Gitee: https://gitee.com/zsf90
 * @website: https://fy2008.github.io
 * @FilePath: \74HC595D\Core\Inc\led74hc595.h
 * @Date: 2021-02-10 18:04:45
 * @LastEditTime: 2021-02-10 21:25:28
 * @LastEditors: Please set LastEditors
 * @Copyright(C): 信念D力量 (freerealmshn@163.com)
 * All Rights Reserved.
 * ----------------------------------------------
 * @Description: 
 * ----------------------------------------------
 * 
 */
#ifndef _LED_74HC595D_H
#define _LED_74HC595D_H

#include "main.h"

/* 数码管位枚举*/
typedef enum {
    GE_BIT,
    SHI_BIT,
    BAI_BIT,
    QIAN_BIT,
    WAN_BIT,
    SHIWAN_BIT,
    BAIWAN_BIT,
    QIANWAN_BIT
}LedBIT;

extern uint8_t data[];
extern SPI_HandleTypeDef hspi5;
void hc74595_display(uint32_t);
#endif