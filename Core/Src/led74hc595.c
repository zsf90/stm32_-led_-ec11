/*
 * @Author: 信念D力量
 * @Github: https://www.github.com/fy2008
 * @Gitee: https://gitee.com/zsf90
 * @website: https://fy2008.github.io
 * @FilePath: \74HC595D\Core\Src\led74hc595.c
 * @Date: 2021-02-10 18:05:00
 * @LastEditTime: 2021-02-10 21:25:38
 * @LastEditors: Please set LastEditors
 * @Copyright(C): 信念D力量 (freerealmshn@163.com)
 * All Rights Reserved.
 * ----------------------------------------------
 * @Description: 
 * ----------------------------------------------
 * 
 */
#include "led74hc595.h"

void hc74595_display(uint32_t num){
    uint32_t qianwan = num / 10000000;       // 解析后的千万位数字
    uint32_t baiwan = num / 1000000 % 10;   // 解析后的百万位数字
    uint32_t shiwan = num / 100000 % 10;    // 解析后的十万位数字
    uint32_t wan = num / 10000 % 10;        // 解析后的万位数字
    uint32_t qian = num / 1000 % 10;        // 解析后的千位数字
    uint32_t bai = num / 100 % 10;          // 解析后的百位数字
    uint32_t shi = num /10 %10;             // 解析后的十位数字
    uint32_t ge = num %10;                  // 解析后的个位数字

    for (uint8_t j=0;j<8;j++){
      
      switch (j){
        case GE_BIT:
          // 个位
          HAL_SPI_Transmit(&hspi5, &data[ge], 1, 1000);     // SPI 输出
          break;
        case SHI_BIT:
          // 十位
          HAL_SPI_Transmit(&hspi5, &data[shi], 1, 1000);
          break;
        case BAI_BIT:
          // 百位
          HAL_SPI_Transmit(&hspi5, &data[bai], 1, 1000);
          break;
        case QIAN_BIT:
          // 千位
          HAL_SPI_Transmit(&hspi5, &data[qian], 1, 1000);
          break;
        case WAN_BIT:
          // 万位
          HAL_SPI_Transmit(&hspi5, &data[wan], 1, 1000);
          break;
        case SHIWAN_BIT:
          // 十万位
          HAL_SPI_Transmit(&hspi5, &data[shiwan], 1, 1000);
          break;
        case BAIWAN_BIT:
          // 万位
          HAL_SPI_Transmit(&hspi5, &data[baiwan], 1, 1000);
          break;
        case QIANWAN_BIT:
          // 万位
          HAL_SPI_Transmit(&hspi5, &data[qianwan], 1, 1000);
          break;
        default:
          break;
      }
    } // end for

    /* 让锁存输出一个高电平脉冲 */
    HAL_GPIO_WritePin(LED_LOAD_GPIO_Port, LED_LOAD_Pin, GPIO_PIN_SET);
    HAL_Delay(1);
    HAL_GPIO_WritePin(LED_LOAD_GPIO_Port, LED_LOAD_Pin, GPIO_PIN_RESET);
}
